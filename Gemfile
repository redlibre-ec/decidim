# frozen_string_literal: true

source "https://rubygems.org"

ruby RUBY_VERSION

gem "decidim", "0.21.0"
gem "decidim-initiatives", "0.21.0"
gem "decidim-consultations", "0.21.0"
gem "decidim-conferences", "0.21.0"
gem 'decidim-jitsi_meetings', git: 'https://github.com/alabs/decidim-module-jitsi-meetings.git'
# gem 'decidim-navbar_links', git: "https://github.com/OpenSourcePolitics/decidim-module-navbar_links", branch: "0.19-stable"
# gem "decidim-calendar", git: "https://github.com/alabs/decidim-module-calendar", branch: "0.17-stable"
# gem "decidim-term_customizer", git: "https://github.com/mainio/decidim-module-term_customizer", branch: "0.19-stable"

gem "airbrake", "~> 5.0"
gem "bootsnap", "~> 1.3"
gem "deface", "~> 1.4"
gem "faker", "~> 1.9"
gem "figaro", "~> 1.1"
gem "puma", ">= 4.3"
gem "redis-namespace"
gem "sidekiq", "~> 5.2"
gem "uglifier", "~> 4.1"

group :development, :test do
  gem "byebug", "~> 11.0", platform: :mri
  gem "rubocop","~> 0.71.0", require: false

  gem "decidim-dev", "0.21.0"
end

group :development do
  gem "capistrano", "~> 3.10"
  gem "capistrano-rails", "~> 1.4"
  gem "capistrano3-puma" 
  gem "capistrano-rbenv", "~> 2.1"
  gem "capistrano-sidekiq", "~> 1.0"
  gem "letter_opener_web", "~> 1.3"
  gem "listen", "~> 3.1"
  gem "spring", "~> 2.0"
  gem "spring-watcher-listen", "~> 2.0"
  gem "web-console", "~> 3.5"
end
