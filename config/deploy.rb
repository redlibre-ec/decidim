# config valid for current version and patch releases of Capistrano
lock "~> 3.14.1"

set :application, "decidim_redlibre_ec"
set :repo_url, "git@gitlab.com:redlibre-ec/decidim.git"

set :deploy_to, "/home/decidim/app"

set :init_system, :systemd
set :rbenv_type, :user
set :delayed_job_workers, 1
set :rbenv_ruby, "2.6.3"
set :sidekiq_config, "config/sidekiq.yml"

append :linked_files, "config/application.yml"
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system", "public/uploads"


set :puma_bind, "tcp://127.0.0.1:3000"
set :puma_user, fetch(:user)
append :rbenv_map_bins, 'puma', 'pumactl'

